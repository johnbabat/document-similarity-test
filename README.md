# Document Similarity

The app is for finding the most similar court cases for a given court case or facts of a case.

## Getting started

### Pre-requisites and Local development

Developers working on this project should have Python3 and pip installed on their local machines

From the base folder run
```
pip install -r requirements.txt
```

To run the application

Create a config.py that contains the directory of datasets.
See config.py.example for a sample

On Linux:
```
export FLASK_ENV=development
flask run
```

On Windows:
```
set FLASK_ENV=development
flask run
```

The application is run on http://0.0.0.0:8000/ by default

## Endpoints

### GET /
- General:
    - Index route, returns text "welcome to document simarity model"

### POST /similarities/facts
- General:
    - Request should be made with just facts of a case as content
    - Returns a list of cases, with a maximum number of 10 cases
- Sample Request:

```
curl \
-X POST \
-F "content=The facts of the case were that the Accused was alleged to have stabbed the deceased to death and thereby murdered him. At the conclusion of trial, the learned trial judge found the Accused guilty for the offence of murder and was accordingly convicted." \
http://127.0.0.1:8000/similarities/generic-text
```

- Sample Result

```
{
    "data": [
        {
            "percentage": 99,
            "subject_matter": "OFFENCE OF MURDER",
            "suit_number": "CA/103/2001"
        },
        {
            "percentage": 97,
            "subject_matter": "OFFENCE OF MURDER",
            "suit_number": "CA/A/789/2016"
        },
        {
            "percentage": 96,
            "subject_matter": "OFFENCE OF MURDER",
            "suit_number": "CA/301B/2013"
        },
        {
            "percentage": 96,
            "subject_matter": "OFFENCE OF CULPABLE HOMICIDE",
            "suit_number": "CA/A/263C1/2013"
        },
        {
            "percentage": 96,
            "subject_matter": "OFFENCE OF MURDER",
            "suit_number": "CA/A/276C/2009"
        },
        {
            "percentage": 96,
            "subject_matter": "OFFENCE OF RAPE/CULPABLE HOMICIDE PUNISHABLE WITH DEATH",
            "suit_number": "CA/A/207C/2012"
        },
        {
            "percentage": 96,
            "subject_matter": "OFFENCE OF ARMED ROBBERY",
            "suit_number": "CA/A/40/20009"
        },
        {
            "percentage": 95,
            "subject_matter": "OFFENCE OF ARMED ROBBERY/CULPABLE HOMICIDE PUNISHABLE WITH DEATH",
            "suit_number": "CA/A/50C/2010"
        },
        {
            "percentage": 95,
            "subject_matter": "OFFENCE OF CONSPIRACY TO COMMIT ARMED ROBBERY/ARMED ROBBERY",
            "suit_number": "CA/A/84/2007"
        },
        {
            "percentage": 95,
            "subject_matter": "OFFENCE OF CONSPIRACY TO COMMIT ARMED ROBBERY/ARMED ROBBERY",
            "suit_number": "CA/A/842/2009"
        }
    ]
}
```

### POST /similarities/generic-text
- General:
    - Request can be made with full court data as content
    - Returns a list of cases, with a maximum number of 10 cases
- Sample Request:

```
curl \
-X POST \
-F "content={Court data text goes in here}" \
http://127.0.0.1:8000/similarities/generic-text
```

- Sample Result:

```
{
    "data": [
        {
            "percentage": 99,
            "subject_matter": "OFFENCE OF MURDER",
            "suit_number": "CA/103/2001"
        },
        {
            "percentage": 97,
            "subject_matter": "OFFENCE OF MURDER",
            "suit_number": "CA/A/789/2016"
        },
        {
            "percentage": 96,
            "subject_matter": "OFFENCE OF MURDER",
            "suit_number": "CA/301B/2013"
        },
        {
            "percentage": 96,
            "subject_matter": "OFFENCE OF CULPABLE HOMICIDE",
            "suit_number": "CA/A/263C1/2013"
        },
        {
            "percentage": 96,
            "subject_matter": "OFFENCE OF MURDER",
            "suit_number": "CA/A/276C/2009"
        },
        {
            "percentage": 96,
            "subject_matter": "OFFENCE OF RAPE/CULPABLE HOMICIDE PUNISHABLE WITH DEATH",
            "suit_number": "CA/A/207C/2012"
        },
        {
            "percentage": 96,
            "subject_matter": "OFFENCE OF ARMED ROBBERY",
            "suit_number": "CA/A/40/20009"
        },
        {
            "percentage": 95,
            "subject_matter": "OFFENCE OF ARMED ROBBERY/CULPABLE HOMICIDE PUNISHABLE WITH DEATH",
            "suit_number": "CA/A/50C/2010"
        },
        {
            "percentage": 95,
            "subject_matter": "OFFENCE OF CONSPIRACY TO COMMIT ARMED ROBBERY/ARMED ROBBERY",
            "suit_number": "CA/A/84/2007"
        },
        {
            "percentage": 95,
            "subject_matter": "OFFENCE OF CONSPIRACY TO COMMIT ARMED ROBBERY/ARMED ROBBERY",
            "suit_number": "CA/A/842/2009"
        }
    ]
}
```

### POST /model-records
- General:
    - Requires username and password authorization
    - Request is made with updated 'AI_LABELLING.csv' file or 'standardization.csv' file or both of them.
    - Updates the 'Merged_Dataset_Standardized.csv' file and the pickle files for the models
    - Returns success value and message

- Sample request:
    `curl -X POST -F "label=@C:\Users\USER\Downloads\AI_LABELLING.csv" -F "standardized=@C:\Users\USER\Downloads\standardization.csv" http://127.0.0.1:8000/model-records`

- Sample result:
```
    {
        'success': True,
        'message': 'File(s) and models updated'
    }
```
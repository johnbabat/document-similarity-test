import os
import gensim
import pickle
import pandas as pd
import numpy as np
import preprocessdata

vector_size = 24
min_count = 2
epochs = 128
seed = 128
workers = 1

param = [24, 2, 128, 128,1]

def tagged_document(list_of_list_of_words):
   for i, list_of_words in enumerate(list_of_list_of_words):
      yield gensim.models.doc2vec.TaggedDocument(list_of_words, [i])


def train_doc2vec_generic_text(merged_dataset):
    list_of_content_wordlist = [data.split() for data in merged_dataset["CONTENT"]]
    d2v_training_data = list(tagged_document(list_of_content_wordlist))

    # Use seed and 1 worker to get deterministic reproduceable results
    d2v_model = gensim.models.doc2vec.Doc2Vec(vector_size=20, min_count=2, epochs=40, seed=128, workers=1)
    d2v_model.build_vocab(d2v_training_data)
    d2v_model.train(d2v_training_data, total_examples=d2v_model.corpus_count, epochs=d2v_model.epochs)

    return d2v_model

def train_doc2vec_facts(merged_dataset):
    list_of_content_wordlist = [data.split() for data in merged_dataset["FACTS ALT"]]
    d2v_training_data = list(tagged_document(list_of_content_wordlist))

    # Use seed and 1 worker to get deterministic reproduceable results
    d2v_model = gensim.models.doc2vec.Doc2Vec(vector_size = 24, min_count = 2, epochs = 128, seed = 128, workers = 1)
    d2v_model.build_vocab(d2v_training_data)
    d2v_model.train(d2v_training_data, total_examples=d2v_model.corpus_count, epochs=d2v_model.epochs)

    return d2v_model

def update(PATH_TO_DATASET):
    # Import necessary datasets
    labelled_dataset = pd.read_csv(os.path.join(PATH_TO_DATASET, 'AI_LABELLING.csv'))
    standardized_dataset = pd.read_csv(os.path.join(PATH_TO_DATASET, 'Standardization.csv'))
    unlabelled_dataset = pd.read_csv(os.path.join(PATH_TO_DATASET, 'full_clean_lawpavilion_database.csv'))


    # Preprocess labelled dataset
    print('Processing data...')
    labelled_dataset.drop(0, inplace=True)
    labelled_dataset.replace("", float("NaN"), inplace=True)
    labelled_dataset = labelled_dataset[['SUIT NUMBER', 'SUBJECT MATTER', "FACTS ALT"]]
    labelled_dataset.dropna(axis=0, inplace=True)

    # Remove invalid suit numbers and duplicates
    labelled_dataset = labelled_dataset[~(labelled_dataset["SUIT NUMBER"].str.contains(" ") & ~labelled_dataset["SUIT NUMBER"].str.contains(" \("))]
    labelled_dataset.drop_duplicates(subset=["SUIT NUMBER"], keep='first', inplace=True)
    labelled_dataset.reset_index(drop=True, inplace=True)

    # Preprocess standardized dataset
    standardized_dataset.replace("", float("NaN"), inplace=True)
    standardized_dataset.dropna(inplace=True)
    standardized_dataset.reset_index(drop=True, inplace=True)

    # Create dictionary mapping old subject matters to standardized subject matters
    old_to_new_map = {}
    for i in range(len(standardized_dataset)):
        old_to_new_map[standardized_dataset["Subject Matter"].iloc[i]] = standardized_dataset["Standardized Version"].iloc[i]

    
    for i in range(len(labelled_dataset)):
        # Update labelled dataset with new suit numbers
        SM = labelled_dataset["SUBJECT MATTER"].iloc[i]
        if SM in old_to_new_map:
            labelled_dataset["SUBJECT MATTER"].iloc[i] = old_to_new_map[SM]

        # Clean facts
        facts = labelled_dataset.iloc[i]["FACTS ALT"]
        labelled_dataset.iloc[i]["FACTS ALT"] = preprocessdata.clean_facts(facts)


    print('Merging datasets...')
    # Merge labelled dataset and unlabelled dataset on suit numbers
    merged_dataset = pd.merge(unlabelled_dataset, labelled_dataset, on='SUIT NUMBER')


    # Save merged dataset
    merged_dataset.to_csv(os.path.join(PATH_TO_DATASET, 'Merged_Dataset_Standardized.csv'), index=False)

    print('Training models...')
    # Train model
    model1 = train_doc2vec_generic_text(merged_dataset)
    model2 = train_doc2vec_facts(merged_dataset)

    print('Training complete. Saving models...')
    # Update models pickle
    with open('d2v_model_pickle.pickle', 'wb') as f:
        pickle.dump(model1, f)

    with open('d2v_model_pickle_2.pickle', 'wb') as f1:
        pickle.dump(model2, f1)